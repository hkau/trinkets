import type { PageLoad } from "./$types";

import type PocketBase from "pocketbase";
import { Record } from "pocketbase";

import init from "$lib/actions/init__pocketBase";

export const load = (async (x) => {
	// create pocketbase client
	const pb: PocketBase = init().pb;

	// get user
	let user: Record = new Record();

	try {
		user = (
			await pb.collection("users").getList(1, 1, {
				filter: `username = "${x.params.username}"`
			})
		).items[0];
	} catch {
		console.log("Failed to load user!");
	}

	// get notes
	let notes: Record[] = [];

	try {
		notes = (
			await pb.collection("notes").getList(1, 50, {
				filter: `owner = "${user.id}" && private = false`
			})
		).items;
	} catch {
		notes = [];
	}

	// return
	return {
		reauth: !(pb.authStore.model !== null),
		username: x.params.username,
		notes,
		user,
		pb
	};
}) satisfies PageLoad;
