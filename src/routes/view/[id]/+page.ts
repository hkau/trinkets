import type { PageLoad } from "./$types";

import type PocketBase from "pocketbase";
import { Record } from "pocketbase";

import init from "$lib/actions/init__pocketBase";

export const load = (async (x) => {
	// create pocketbase client
	const pb: PocketBase = init().pb;

	// create note
	let note: Record = new Record();

	// load notes
	try {
		note = await pb.collection("notes").getOne(x.params.id, {
			"expand": "owner"
		});
	} catch {
		note = new Record({
			title: "Failed to load note!",
			content: '<h1 align="center">Failed to load note!</h1><a href="/">Return Home</a>',
			id: "0"
		});
	}

	// return
	return {
		note,
		reauth: !(pb.authStore.model !== null),
		id: x.params.id,
		pb
	};
}) satisfies PageLoad;
