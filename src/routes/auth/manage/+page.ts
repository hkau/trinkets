import type { PageLoad } from "./$types";

import type PocketBase from "pocketbase";
import init from "$lib/actions/init__pocketBase";

export const load = (async () => {
	// create pocketbase client
	const pb: PocketBase = init().pb;

	// return
	return {
		reauth: !(pb.authStore.model !== null),
		pb
	};
}) satisfies PageLoad;
