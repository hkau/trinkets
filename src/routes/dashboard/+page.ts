import type { PageLoad } from "./$types";

import type PocketBase from "pocketbase";
import type { Record } from "pocketbase";

import init from "$lib/actions/init__pocketBase";

export const load = (async () => {
	// create pocketbase client
	const pb: PocketBase = init().pb;

	// create notes
	let notes: Record[] = [];

	// load notes
	if (pb.authStore.model)
		try {
			notes = await pb.collection("notes").getFullList({
				filter: `owner = "${pb.authStore.model.id}"`
			});
		} catch {
			alert("Failed to run action!");
		}

	// return
	return {
		notes,
		reauth: !(pb.authStore.model !== null),
		pb
	};
}) satisfies PageLoad;
