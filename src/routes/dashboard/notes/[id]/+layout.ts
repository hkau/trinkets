import type { LayoutLoad } from "./$types";

import type PocketBase from "pocketbase";
import { Record } from "pocketbase";

import init from "$lib/actions/init__pocketBase";

export const load = (async (x) => {
	// create pocketbase client
	const pb: PocketBase = init().pb;

	// create note
	let note: Record = new Record();

	// load notes
	if (pb.authStore.model)
		try {
			note = await pb.collection("notes").getOne(x.params.id);
		} catch {
			alert("Failed to run action!");
		}

	// return
	return {
		note,
		reauth: !(pb.authStore.model !== null),
		id: x.params.id,
		pb
	};
}) satisfies LayoutLoad;
