import PocketBase from "pocketbase";
import site from "../site.json";

export default function init(): any {
	// this isn't federated, we only need to return a pocketbase client!
	const pb = new PocketBase(site.backend);
	return { pb };
}
