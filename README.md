<h1 align="center">📚 Trinkets</h1>
<h2 align="center">The open-source notes platform</h2>

## What is Trinkets?

Trinkets allows you to save notes written in Markdown and view them from any device.

## Features

- Save notes in named items
- Write notes in Markdown
- Display a preview of your rendered Markdown notes while editing
- Easy to host yourself, [PocketBase](https://pocketbase.io) backend
  - PocketBase schema at `./pocketbase/pb_schema.json`
  - Edit your deployed client `site.json` and change the value of `backend` to the URL of your PocketBase instance
